from abc import ABC, abstractmethod

class Person(ABC):
    @abstractmethod
    def getFullName(self): 
        pass

    @abstractmethod
    def addRequest(self, request):
        pass

    @abstractmethod
    def checkRequest(self, request):
        pass

    @abstractmethod
    def addUser(self, user):
        pass

class Employee(Person):
    def __init__(self, firstName, lastName, email, department):
        self.__firstName = firstName
        self.__lastName = lastName
        self.__email = email
        self.__department = department
    
    def get_firstName(self):
        return self.__firstName
    
    def set_firstName(self, firstName):
        self.__firstName = firstName

    def get_lastName(self):
        return self.__lastName
    
    def set_lastName(self, lastName):
        self.lastName = lastName
    
    def get_email(self):
        return self.__email
    
    def set_email(self, email):
        self.__email = email

    def get_department(self):
        return self.__department
    
    def set_department(self, department):
        self.__department = department

    def getFullName(self):
        # print(f"{self.__firstName} {self.__lastName}")
        return self.__firstName + " " + self.__lastName 

    def addRequest(self):
        return "Request has been added"
        
    def checkRequest(self, request):
        pass

    def addUser(self, user):
        pass
    
    def login(self):
        return self.__email + " has logged in"
    
    def logout(self):
        return self.__email + " has logged out"

class TeamLead(Person):
    def __init__(self, firstName, lastName, email, department):
        self.__firstName = firstName
        self.__lastName = lastName
        self.__email = email
        self.__department = department
        
        self.__members = []

    def get_firstName(self):
        return self.__firstName
    
    def set_firstName(self, firstName):
        self.__firstName = firstName

    def get_lastName(self):
        return self.__lastName
    
    def set_lastName(self, lastName):
        self.__lastName = lastName
    
    def get_email(self):
        return self.__email
    
    def set_email(self, email):
        self.__email = email

    def get_department(self):
        return self.__department
    
    def set_department(self, department):
        self.__department = department

    def get_members(self):
        return self.__members
    
    def set_members(self, members):
        self.__members = members
    
    def getFullName(self):
        return self.__firstName + " " + self.__lastName 
    
    def addRequest(self, request):
        pass

    def checkRequest(self, request):
        return "Your request has been checked and processed" 
    
    def addUser(self, user):
        pass

    def login(self):
        return self.__email + " has logged in"

    def logout(self):
        return self.__email + " has logged out"
    
    def addMember(self, employee):
        return self.__members.append(employee)

class Admin(Person):
    def __init__(self, firstName, lastName, email, department):
        self.__firstName = firstName
        self.__lastName = lastName
        self.__email = email
        self.__department = department

    def get_firstName(self):
        return self.__firstName
    
    def set_firstName(self, firstName):
        self.__firstName = firstName

    def get_lastName(self):
        return self.__lastName
    
    def set_lastName(self, lastName):
        self.lastName = lastName
    
    def get_email(self):
        return self.__email
    
    def set_email(self, email):
        self.__email = email

    def get_department(self):
        return self.__department
    
    def set_department(self, department):
        self.__department = department

    
    def getFullName(self):
        return self.__firstName + " " + self.__lastName 
    
    def addRequest(self, request):
        pass

    def checkRequest(self, request):
        pass

    def addUser(self):
            # print("New user added")
        return "User has been added"

    def login(self):
        return self.__email + " has logged in"
    
    def logout(self):
        return self.__email + " has logged out"

class Request():
    def __init__(self, name, requester, dateRequested):
        self.__name = name
        self.__requester = requester
        self.__dateRequested = dateRequested
        
        self.__status = ""

    def get_status(self):
        return self.__status
    
    def set_status(self, status_string):
        self.__status = status_string
    
    def updateRequest(self):
        return f"Request {self.__name} has been updated"
    
    def closeRequest(self):
        return f"Request {self.__name} has been closed"

    def cancelRequest(self):
        return f"Request {self.__name} has been cancelled"






# Test cases ------------------------------------------------------

employee1 = Employee("John", "Doe", "djohn@mail.com", "Marketing")
employee2 = Employee("Jane", "Smith", "sjane@mail.com", "Marketing")
employee3 = Employee("Robert", "Patterson", "probert@mail.com", "Sales")
employee4 = Employee("Brandon", "Smith", "sbrandon@mail.com", "Marketing")

admin1 = Admin("Monika", "Justin", "jmonika@mail.com", "Marketing")
teamLead1 = TeamLead("Michael", "Specter", "smichael@mail.com", "Sales")

req1 = Request("New hire orienatation", teamLead1, "27-Jul-2021")
req2 = Request("Laptop Repair", employee1, "1-Jul-2021")

assert employee1.getFullName() == "John Doe", "Full name should be John Doe"
assert admin1.getFullName() == "Monika Justin", "Full name should be Monika Justin"
assert teamLead1.getFullName() == "Michael Specter", "Full name should be Michael Specter"
assert employee2.login() == "sjane@mail.com has logged in"
assert employee2.addRequest() == "Request has been added"
assert employee2.logout() == "sjane@mail.com has logged out"

teamLead1.addMember(employee3)
teamLead1.addMember(employee4)

for indiv_emp in teamLead1.get_members():
    print(indiv_emp.getFullName())

assert admin1.addUser() == "User has been added"

req2.set_status("closed")
print(req2.closeRequest())